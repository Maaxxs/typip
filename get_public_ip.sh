#!/bin/bash

# Crontab entry
# @reboot /home/your-username/.scripts/get_public_ip.sh

# Put this script in /home/$USER/.scripts

#
# This script pulls your IP address once a day.
#

USER=$USER

function get_ip {
    # Getting desktop ready until curl is available
    echo 'Waiting for 5 minutes'
    sleep 300
    echo '[*] Downloading IP'
    date | tr -d '\n' >> /home/$USER/.scripts/public_ips
    echo -n ' - ' >> /home/$USER/.scripts/public_ips
    curl -s https://ipinfo.io/ip >> /home/$USER/.scripts/public_ips
    echo '[+] Done.'
}


cd /home/$USER/.scripts

if [ ! -f ./public_ips ]
then
    # file doesn't exist yet
    get_ip
elif ! tail -n1 ./public_ips | grep --quiet "$(date | awk {'print $1,$2,$3'})"
then
    # IP was not yet downloaded for today
    get_ip
else
    echo '[*] IP was already downloaded today.'
fi


